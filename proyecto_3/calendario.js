var months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto",
				"Septiembre", "Octubre", "Noviembre", "Diciembre"];
$(document).ready(function()  {
    $('#selec_fecha').datepicker({
      dateFormat: 'dd-mm-yy',	
      inline: true,
      onSelect: function() { 
      		var bool = true;
      		var date = null;
            date = $(this).datepicker('getDate'),
            day  = date.getDate(),
            month = date.getMonth() + 1,              
            year =  date.getFullYear();
        	$('#btn_convertir').click(function(){
		    	//var date = new Date();
		    	//date = document.getElementById('selec_fecha').value;
		    	if(bool)
		    	{
			    	var time = new Date();
			    	var hour = (time.getHours() >= 12)? (time.getHours()-12) : time.getHours();
			    	var full_hour = hour + ":" + time.getMinutes() + ((time.getHours() >= 12)? ' pm' : ' am'); 
			    	alert(day + ' de ' + months[month-1] + ' de ' + year + " a las " + full_hour);
			    	document.getElementById("selec_fecha").value = "";
			    	bool = false;
		    	}

    		});
     	}	 
    });
});